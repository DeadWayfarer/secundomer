/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secundomer;

import java.awt.Color;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class TimeSelectDialog extends javax.swing.JDialog {
    public int time;
    public TimeSelectDialog(int time) {
        super((java.awt.Frame) null, true);
        initComponents();
        setLocationRelativeTo(null);
        setAlwaysOnTop(true);
        getContentPane().setBackground(Color.WHITE);
        setIconImage(SecundomerForm.icon);
        
        sSpn.setModel(getModel());
        mSpn.setModel(getModel());
        hSpn.setModel(getModel());
        
        this.time = time;
        int h = time / 3600;
        int m = time % 3600 / 60;
        int s = time % 60;
        
        hSpn.setValue(h);
        mSpn.setValue(m);
        sSpn.setValue(s);
    }
    
    public static int getTime(int time) {
        TimeSelectDialog d = new TimeSelectDialog(time);
        d.show();
        return d.time;
    }
    
    public SpinnerNumberModel getModel() {
        return new SpinnerNumberModel(0, 0, 59, 1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        timeLabel = new javax.swing.JLabel();
        mSpn = new javax.swing.JSpinner();
        timeLabel1 = new javax.swing.JLabel();
        sSpn = new javax.swing.JSpinner();
        timeLabel2 = new javax.swing.JLabel();
        commit = new javax.swing.JButton();
        hSpn = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ввод времени");

        timeLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        timeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timeLabel.setText("ч");

        mSpn.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        timeLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        timeLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timeLabel1.setText("м");

        sSpn.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        timeLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        timeLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timeLabel2.setText("с");

        commit.setBackground(new java.awt.Color(51, 153, 0));
        commit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        commit.setText("Подтвердить");
        commit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                commitActionPerformed(evt);
            }
        });

        hSpn.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(hSpn, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(timeLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mSpn, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(timeLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sSpn, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(timeLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(commit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(timeLabel)
                    .addComponent(timeLabel1)
                    .addComponent(mSpn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeLabel2)
                    .addComponent(sSpn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hSpn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(commit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void commitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_commitActionPerformed
        int h = (int) hSpn.getValue();
        int m = (int) mSpn.getValue();
        int s = (int) sSpn.getValue();
        
        time = h*3600+m*60+s;
        dispose();
    }//GEN-LAST:event_commitActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton commit;
    private javax.swing.JSpinner hSpn;
    private javax.swing.JSpinner mSpn;
    private javax.swing.JSpinner sSpn;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel timeLabel1;
    private javax.swing.JLabel timeLabel2;
    // End of variables declaration//GEN-END:variables
}
