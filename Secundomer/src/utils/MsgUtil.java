/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import javax.swing.JOptionPane;

/**
 * Утилита для вывода сообщений пользователю.
 * @author ДемоЭкзамен
 */
public class MsgUtil {
    /**
     * Показывает сообщение пользователю.
     * @param msg - сообщение которое покажется пользователю.
     */
    public static void message(String msg) {
        JOptionPane.showMessageDialog(null, msg,"Сообщение",1);
    }
    
    /**
     * Показывает диалог для ввода 
     * @param msg сообщение
     * @param title заголовок диалога
     * @return Возвращает значение введенное пользователем
     */
    public static String input(String msg, String title) {
        return JOptionPane.showInputDialog(null, msg, title, JOptionPane.QUESTION_MESSAGE);
    }
    
    /**
     * Показывает форму для подтверждения
     * @param msg - сообщение
     * @return Возвращает true если пользователь нажал "Да"
     */
    public static boolean confirm(String msg) {
        String[] o = {"Да","Нет"};
        return JOptionPane.showOptionDialog(null, msg, "Подтверждение", 0, 2, null, o, null) == 0;
    }
}
