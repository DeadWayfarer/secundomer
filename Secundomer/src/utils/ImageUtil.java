/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

/**
 * Утилита для работы с изображением
 * @author ДемоЭкзамен
 */
public class ImageUtil {
    
    public static BufferedImage imageFromFile(String filePath) {
        File file = new File(filePath);
        BufferedImage image = null;
        try {
            image = ImageIO.read(file);
        } catch (IOException ex) {
            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
            MsgUtil.message("Не удалось загрузить файл!");
        }
        return image;
    }
    
    public static BufferedImage imageFromFile(File file) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(file);
        } catch (IOException ex) {
            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
            MsgUtil.message("Не удалось загрузить файл!");
        }
        return image;
    }
    
    /**
     * Преобразует изображение в тип ImageIcon, для отображения в JLabel,
     * автоматически сжимая до размера 100x100
     * @param image
     * @return 
     */
    public static ImageIcon toIcon(BufferedImage image) {
        return toIcon(image,100,100);
    }
    
    /**
     * Преобразует изображение в тип ImageIcon, для отображения в JLabel,
     * автоматически сжимая до размера w на h
     * @param image
     * @param w
     * @param h
     * @return 
     */
    public static ImageIcon toIcon(BufferedImage image, int w, int h) {
        if (image == null) {
            return new ImageIcon();
        }
        return new ImageIcon(image.getScaledInstance(w,h,1));
    }
}
